import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

class App2 extends React.Component {
    render() {
        return (

            <View style={styles.container} >
                <View style={styles.header}>
                    <View style={styles.select}>
                        <Text style={styles.textStyle}>=</Text>
                    </View>
                    <View style={styles.boxHearder}>
                        <Text style={styles.textStyle}>Header</Text>
                    </View>
                    <View style={styles.icon}>
                        <Text style={styles.textStyle}>x</Text>
                    </View>
                </View>

                <View style={[styles.content, styles.center]}>
                    <Text style={styles.textStyle}>scrollView</Text>
                </View>

                <View style={styles.footer}>
                    <View style={styles.header}>
                        <View style={styles.select}>
                            <Text style={styles.textStyle}>I</Text>
                        </View>

                        <View style={styles.select}>
                            <Text style={styles.textStyle}>C</Text>
                        </View>

                        <View style={styles.select}>
                            <Text style={styles.textStyle}>O</Text>
                        </View>

                        <View style={styles.select}>
                            <Text style={styles.textStyle}>N</Text>
                        </View>
                    </View>

                </View>
            </View>


        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: '#CD6155',
        alignItems: 'center',
        flexDirection: 'row'
    },

    boxHearder: {
        backgroundColor: '#A3E4D7',
        flex: 1,
        margin: 4,
        padding: 20


    },

    select: {
        backgroundColor: '#A3E4D7',
        flex: 0.3,
        margin: 4,
        padding: 20

    },

    icon: {
        backgroundColor: '#A3E4D7',
        flex: 0.3,
        margin: 4,
        padding: 20

    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },

    footer: {
        backgroundColor: '#CD6155',
        alignItems: 'center',
        flexDirection: 'row'
    },


    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    textStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 30,
    }

})
export default App2

