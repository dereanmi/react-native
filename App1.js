import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

class App1 extends React.Component {
    render() {
        return (
            <View style={styles.container} >
                <View style={styles.content}>
                    <View style={[styles.layout1,styles.center]}>
                        <View style={[styles.image,styles.logo,styles.center]}>
                            <Text style=
                                {{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontSize: 20,
                                }}>Image</Text>
                        </View>
                    </View>

                    <View style={styles.layout2}>
                        <View style={[styles.textInput1,styles.center]}>
                            <Text style=
                                {{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontSize: 20,
                                }}>TextInput</Text>
                        </View>

                        <View style={[styles.textInput2,styles.center]}>
                            <Text style=
                                {{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontSize: 20,
                                }}>TextInput</Text>
                        </View>

                        <View style={[styles.touchable,styles.center]}>
                            <Text style=
                                {{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontSize: 20,
                                    
                                }}>TouchableOpacity</Text>
                        </View>

                    </View>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },

    layout1: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },

    layout2: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    }, 

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 100,
        width: 200,
        height: 200
    },
    
    image: {
        backgroundColor: '#7DCEA0',
        margin: 50,
    },

    textInput1: {
        backgroundColor: '#7DCEA0',
        flex: 1,
        padding: 12,
        margin: 6
    },
    
    textInput2: {
        backgroundColor: '#7DCEA0',
        flex: 1,
        padding: 12,
        margin: 6
    },

    touchable: {
        backgroundColor: '#7DCEA0',
        flex: 1,
        margin: 70,
    }
})
export default App1
