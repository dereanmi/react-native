import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

class App extends React.Component {
  render() {
    return (

      <View style={styles.container} >
        <View style={styles.header}>
          <Text style={styles.headerText}>News</Text>
        </View>

        <View style={styles.content}>


          <View style={styles.row}>

            <View style={styles.box1}>
              <Text style=
                {{
                  color: 'white',
                  textAlign: 'center',
                  fontSize: 20,
                }}>Lorem</Text>
            </View>

            <View style={styles.box2}>
              <Text style=
                {{
                  color: 'white',
                  textAlign: 'center',
                  fontSize: 20,
                }}>Lorem</Text>
            </View>
          </View>

          <View style={styles.row}>

            <View style={styles.box1}>
              <Text style=
                {{
                  color: 'white',
                  textAlign: 'center',
                  fontSize: 20,
                }}>Lorem</Text>
            </View>

            <View style={styles.box2}>
              <Text style=
                {{
                  color: 'white',
                  textAlign: 'center',
                  fontSize: 20,
                }}>Lorem</Text>
            </View>
          </View>

          <View style={styles.row}>

            <View style={styles.box1}>
              <Text style=
                {{
                  color: 'white',
                  textAlign: 'center',
                  fontSize: 20,
                }}>Lorem</Text>
            </View>

            <View style={styles.box2}>
              <Text style=
                {{
                  color: 'white',
                  textAlign: 'center',
                  fontSize: 20,
                }}>Lorem</Text>
            </View>
          </View>



        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({

  container: {
    backgroundColor: '#5DADE2',
    flex: 1
  },

  header: {
    backgroundColor: '#C0392B',
    alignItems: 'center'
  },

  headerText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 30
  },

  content: {
    backgroundColor: '#F8C471',
    flex: 1,
    flexDirection: 'column'
  },

  box1: {
    backgroundColor: '#7DCEA0',
    flex: 1,
    margin: 14,
  },

  box2: {
    backgroundColor: '#7DCEA0',
    flex: 1,
    margin: 14,
  },

  row: {
    backgroundColor: '#85C1E9',
    flex: 1,
    margin: 14,
    flexDirection: 'row'
  }

})
export default App
